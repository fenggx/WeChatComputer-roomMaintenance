let icon = {
  // 紧急维修图标
  press: 'https://z3.ax1x.com/2021/09/26/4ys4vF.png',
  // 普通维修图标
  ordinary: 'https://z3.ax1x.com/2021/09/26/4ysf3T.png'
}

let floor = [];

export {
  icon,
  floor
}